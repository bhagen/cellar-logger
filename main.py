import time
import logging
from logging.handlers import SysLogHandler
from datetime import datetime

import Adafruit_DHT
from influxdb import InfluxDBClient
import yaml
from pathlib import Path


class DHTSensor():
    def __init__(self, name: str, sens_type: str, pin: int):
        self.name = name

        if sens_type == 'DHT11':
                self.sens_type = Adafruit_DHT.DHT11
        elif sens_type == 'DHT22':
                self.sens_type = Adafruit_DHT.DHT22
        elif sens_type == 'AM2302':
                self.sens_type = Adafruit_DHT.AM2302
        else:
                logging.error(f'Unknown sensor type: {sens_type}')
                self.sens_type = None

        logging.info(f'Set sensor type to {self.sens_type}')

        self.pin = pin

        self.humidity = None
        self.temp = None

        self.connected = False

    def read(self):
        hum, temp = Adafruit_DHT.read(self.sens_type, self.pin)

        if hum is None or temp is None:
            self.connected = False
            return False
        else:
            self.connected = True
            self.humidity = hum
            self.temp = temp
            return True


class CellarLogger():
    def __init__(self, config_path):
        config_path = Path(config_path)

        self.config = self._read_config(config_path)
        self.sensors = self._create_sensors()
        self.db = self._setup_db_connection(self.config['db_address'], self.config['db_port'],
                                            self.config['db_username'], self.config['db_password'],
                                            self.config['db_database'])
        self.location = self.config['location']
        self.pi_name = self.config['pi_name']

    def _read_config(self, config_path: Path) -> dict:

        with config_path.open('r') as f:
            config = yaml.safe_load(f)

        return config

    def _setup_db_connection(self, address: str, port: int, username: str, password: str, database: str) -> InfluxDBClient:
        client = InfluxDBClient(
            host=address, port=port, username=username, password=password, database=database)

        client.create_database(database)

        return client

    def _create_sensors(self):
        sensors = []
        for sensor_config in self.config['sensors']:
            logging.info(
                f"Creating sensor {sensor_config['name']} with type {sensor_config['type']} connected to pin {sensor_config['pin']}")
            sensors.append(
                DHTSensor(name=sensor_config['name'], sens_type=sensor_config['type'], pin=sensor_config['pin']))

        return sensors

    def _send_measurement(self, sensor_name: str, humidity, temp):
        logging.debug(
            f"Sending measurement for sensor {sensor_name}: Hum: {humidity} Temp: {temp}")
        json_body = [
            {
                "measurement": f"sensor_{sensor_name}",
                "tags": {
                    "host": self.pi_name,
                    "region": self.location
                },
                "time": datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
                "fields": {
                    "humidity": humidity,
                    "temp": temp
                }
            }
        ]

        self.db.write_points(json_body)

    def main(self):
        while True:
            for sensor in self.sensors:
                sensor.read()
                if sensor.connected:
                    self._send_measurement(
                        sensor.name, sensor.humidity, sensor.temp)
                else:
                    logging.warning(
                        f"Sensor {sensor.name} is reporting as not connected. Not sending data")

            logging.debug(f"Sleeping for {self.config['update_rate_s']}s")
            time.sleep(self.config['update_rate_s'])


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)

    formatter = logging.Formatter('python-cellartracker: %(asctime)s - %(name)s - %(levelname)s - %(message)s')
    syslog_handler = SysLogHandler(address='/dev/log')
    syslog_handler.setFormatter(formatter)
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logging.getLogger().addHandler(syslog_handler)
    logging.getLogger().addHandler(console_handler)


    cl = CellarLogger('./config.yaml')

    while True:
        try:
            cl.main()
        except Exception as e:
            logging.error('Unhandled exception! Restarting.', exc_info=True)
