# Example Config File
```
location: nycity
pi_name: downstairs_cellar

db_address: influxdb.fo.bar
db_port: 555
db_database: cellar-logger
db_username: test
db_password: "test"

sensors:
- {name: floor, type: dht22, pin: 23}
- {name: cieling, type: dht22, pin: 33}

update_rate_s: 5
```
